package me.sheimi;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import me.sheimi.sgit.R;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.anything;


@RunWith(AndroidJUnit4.class)
@LargeTest
public class SGitTest {

   @Rule
   public final ActivityTestRule<me.sheimi.sgit.RepoListActivity> main = new ActivityTestRule<>(me.sheimi.sgit.RepoListActivity.class);

   /**
    * Test for SGit
    * @author - glad.pedro@gmail.com
    * Generated using Barista - http://moquality.com/barista
    */
   @Test
   public void test_SGitTest() {
     onView(withId(R.id.action_new)).perform(click());
     onView(withId(R.id.remoteURL)).perform(clearText(), typeText("https://github.com/pedrogyn/Preferencias"));
     onData(anything()).inAdapterView(withId(R.id.repoList)).atPosition(1).check(matches(isDisplayed()));

   }
}